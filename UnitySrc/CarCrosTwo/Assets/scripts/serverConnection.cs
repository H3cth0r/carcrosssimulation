using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Extra libs
using UnityEditor;
using UnityEngine.Networking;

// serialize json objects
using System.Text.Json;



// ==================================================
// ==================================================
// Serialization classes
// ==================================================
// ==================================================

// Server response serialization
[System.Serializable]
public class carsResponse{
    public List<carStatus> cars;
    public List<TFLStatus> tfls;
}
// Car Status Serialization
[System.Serializable]
public class carStatus{
    public string carId;
    public float dir;
    public float x;
    public float y;
}
// Traffic light status Serialization
[System.Serializable]
public class TFLStatus{
    public int light;
    public string tflId;
}



// ==================================================
// ==================================================
// Main class
// ==================================================
// ==================================================
public class serverConnection : MonoBehaviour
{

    // Access link to server
    public string initUrl = "http://192.168.3.24:8000/init";
    public string stepUrl = "http://192.168.3.24:8000/step";

    // Collection of responses
    List<carsResponse> serverResponses = new List<carsResponse>();

    // prefabs lists
    public List<GameObject> prefabsList;
    Dictionary<string, GameObject> currentCars = new Dictionary<string, GameObject>();

    // traffic light prefabs
    public GameObject tf0;
    public GameObject tf1;
    public GameObject tf2;
    public GameObject tf3;
    // traffic lights spotlight
    public Light sptf0;
    public Light sptf1;
    public Light sptf2;
    public Light sptf3;

    // ==================================================
    // New car to scene Method
    // ==================================================
    void addNewCarToScene(string idCar){
        // Select a random prebab
        System.Random random = new System.Random();
        int prefabSelectionNumber = random.Next(prefabsList.Count);
        //int prefabSelectionNumber = 4;
        GameObject newCar = Instantiate(prefabsList[prefabSelectionNumber], new Vector3(0, 0, 0), Quaternion.identity) as GameObject;

        List<carStatus> currList = serverResponses[serverResponses.Count-1].cars;
        for(int i = 0; i < currList.Count; i++){
            if(currList[i].carId == idCar){
                if(currList[i].dir == 1){
                    // Rotation if for vocho and delorean
                    if(prefabSelectionNumber == 7 || prefabSelectionNumber == 4)
                        newCar.transform.rotation = Quaternion.Euler(new Vector3(0,270,0));
                    else 
                        newCar.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
                }
                else if(currList[i].dir == 0){
                    if(prefabSelectionNumber == 7 || prefabSelectionNumber == 4) 
                        newCar.transform.rotation = Quaternion.Euler(new Vector3(0,90,0));
                    else
                        newCar.transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
                }
                else if(currList[i].dir== 3){
                    if(prefabSelectionNumber == 7 || prefabSelectionNumber == 4) 
                        newCar.transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
                    else
                        newCar.transform.rotation = Quaternion.Euler(new Vector3(0,270,0));
                }
                else if(currList[i].dir== 2){
                    if(prefabSelectionNumber == 7 || prefabSelectionNumber == 4) 
                        newCar.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
                    else
                        newCar.transform.rotation = Quaternion.Euler(new Vector3(0,90,0));
                }
            }
        }
        currentCars.Add(idCar, newCar);

        return;
    }


    // ==================================================
    // Async method for movint a car depending on its car
    // Status id
    // ==================================================
    IEnumerator MoveCar(carStatus nextDestination){

        GameObject carObject = currentCars[nextDestination.carId];

        // Save next destiantion as vector
        Vector3 nextDestionationVector = new Vector3(nextDestination.x*10+5, 0.75f, nextDestination.y*10+5);
        // Get the current position of the car
        Vector3 carInitialPos = carObject.transform.position;

        
        // get the distance between two points
        float dist = Vector3.Distance(nextDestionationVector, carInitialPos);

        // if the distance means the car is on the edge, and is about to return to
        // intial position then just make transform position
        if(dist < 200f || dist < -200){

            // counter 
            float dtCounter = 0;

            float startTime = Time.time;
            float duration = 0.4f;

            while(dtCounter < duration){
                carObject.transform.position = Vector3.Lerp(carInitialPos, nextDestionationVector, (Time.time - startTime)/duration);
                dtCounter += Time.deltaTime;
                yield return null;
            }
        }
        Debug.Log(carObject.transform.position);
        carObject.transform.position = nextDestionationVector;
    }

    // ==================================================
    // Method for making requests to the server and getting
    // the status of the objects.
    // ==================================================
    IEnumerator StepData(){
        WWWForm form = new WWWForm();
        string url = stepUrl;
        using(UnityWebRequest www = UnityWebRequest.Get(url)){
            Vector3 fakePos     =   new Vector3(3.44f, 0, -15.707f);
            string data         =   EditorJsonUtility.ToJson(fakePos);
            byte[] bodyRaw      =   System.Text.Encoding.UTF8.GetBytes(data);
            www.uploadHandler   =   (UploadHandler)new UploadHandlerRaw(bodyRaw);
            www.downloadHandler =   (DownloadHandler)new DownloadHandlerBuffer();
            www.downloadHandler =   (DownloadHandler)new DownloadHandlerBuffer();

            yield return www.SendWebRequest();      // request to python
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            } else{
                List<Vector3> newPositions = new List<Vector3>();
                string txt = www.downloadHandler.text.Replace('\'', '\"');
                Debug.Log(txt);
                carsResponse result = JsonUtility.FromJson<carsResponse>(txt);
                serverResponses.Add(result);

                // add car if not exists
                for(int i = 0; i < result.cars.Count; i++){
                    if(!currentCars.ContainsKey(result.cars[i].carId)){
                        // Create new car
                        addNewCarToScene(result.cars[i].carId);
                    }
                }

                Debug.Log(result.cars[0].carId);
            }
        }
    }

    // ==================================================
    // Method for setting cars to init pos
    // ==================================================
    void setInitPosCar(string carId){
        List<carStatus> currList = serverResponses[serverResponses.Count-1].cars;
        GameObject carObject = currentCars[carId];

        for(int i = 0; i < currList.Count; i++){
            if(currList[i].carId == carId){
                carObject.transform.position = new Vector3(currList[i].x * 10 + 5, 0.75f, currList[i].y * 10 + 5);
            }
        }
    }

    // ==================================================
    // Method for making the first request to the server
    // ==================================================
    IEnumerator InitData(){
        WWWForm form = new WWWForm();
        string url = initUrl;
        using(UnityWebRequest www = UnityWebRequest.Get(url)){
            Vector3 fakePos     =   new Vector3(3.44f, 0, -15.707f);
            string data         =   EditorJsonUtility.ToJson(fakePos);
            byte[] bodyRaw      =   System.Text.Encoding.UTF8.GetBytes(data);
            www.uploadHandler   =   (UploadHandler)new UploadHandlerRaw(bodyRaw);
            www.downloadHandler =   (DownloadHandler)new DownloadHandlerBuffer();
            www.downloadHandler =   (DownloadHandler)new DownloadHandlerBuffer();

            yield return www.SendWebRequest();      // request to python
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            } else{
                List<Vector3> newPositions = new List<Vector3>();
                string txt = www.downloadHandler.text.Replace('\'', '\"');
                Debug.Log(txt);
                carsResponse result = JsonUtility.FromJson<carsResponse>(txt);
                serverResponses.Add(result);

                // add car if not exists
                for(int i = 0; i < result.cars.Count; i++){
                    if(currentCars.ContainsKey(result.cars[i].carId) == false){
                        // Create new car
                        addNewCarToScene(result.cars[i].carId);
                    }
                }

                Debug.Log(result.cars[0].carId);
            }
        }
    }


    // ==================================================
    // Method for updating the tra traffic lights status
    // ==================================================
    IEnumerator changeTrafficLightStatus(TFLStatus tffs){
        float emissiveIntensityOn   = 1;
        float emissiveIntensityOff  = 0;

        GameObject stopLight = tf0;
        Light tflSp = sptf0;
        if(int.Parse(tffs.tflId) == 0){ 
            stopLight = tf0;
            tflSp = sptf0;
        }
        else if(int.Parse(tffs.tflId) == 1){
            stopLight = tf1;
            tflSp = sptf1;
        }
        else if(int.Parse(tffs.tflId) == 2){
            stopLight = tf2;
            tflSp = sptf2;
        }
        else if(int.Parse(tffs.tflId) == 3){
            stopLight = tf3;
            tflSp = sptf3;
        }

        var status = tffs.light;

        var mats = stopLight.GetComponent<MeshRenderer>().materials;

        Color ColorGreen            = mats[3].GetColor("_Color");
        Color emissionColorGreen    = mats[3].GetColor("_EmissionColor");
        Color ColorYellow           = mats[1].GetColor("_Color");
        Color emissionColorYellow   = mats[1].GetColor("_EmissionColor");
        Color ColorRed              = mats[2].GetColor("_Color");
        Color emissionColorRed      = mats[2].GetColor("_EmissionColor");


        if(status == 0){        // red

            tflSp.color = ColorGreen;

            mats[3].SetColor("_Color", ColorGreen);
            mats[3].SetColor("_EmissionColor", ColorGreen * emissiveIntensityOn);

            mats[1].SetColor("_Color", ColorYellow);
            mats[1].SetColor("_EmissionColor", ColorYellow * emissiveIntensityOff);

            mats[2].SetColor("_Color", ColorRed);
            mats[2].SetColor("_EmissionColor", ColorRed * emissiveIntensityOff);
        }else if(status == 2){  // Green

            tflSp.color = ColorYellow;

            mats[3].SetColor("_Color", ColorGreen);
            mats[3].SetColor("_EmissionColor", ColorGreen * emissiveIntensityOff);

            mats[1].SetColor("_Color", ColorYellow);
            mats[1].SetColor("_EmissionColor", ColorYellow * emissiveIntensityOn);

            mats[2].SetColor("_Color", ColorRed);
            mats[2].SetColor("_EmissionColor", ColorRed * emissiveIntensityOff);

        }else if(status == 1){  // Yellow

            tflSp.color = ColorRed;

            mats[3].SetColor("_Color", ColorGreen);
            mats[3].SetColor("_EmissionColor", ColorGreen * emissiveIntensityOff);

            mats[1].SetColor("_Color", ColorYellow);
            mats[1].SetColor("_EmissionColor", ColorYellow * emissiveIntensityOff);

            mats[2].SetColor("_Color", ColorRed);
            mats[2].SetColor("_EmissionColor", ColorRed * emissiveIntensityOn);

        }

        stopLight.GetComponent<MeshRenderer>().materials = mats;
        yield return null;
    }



    // ==================================================
    // Here we send the first request to se server to get
    // the initial positions and cars
    // ==================================================
    void Start()
    {
        StartCoroutine(InitData());
    }




    float timeInterval  = 0.4f;         // time interval, one second
    float timeCounter   = 0;         // time counter for execution
    // ==================================================
    // Update is called once per frame
    // ==================================================
    void Update()
    {
        timeCounter += Time.deltaTime;
        while(timeCounter >= timeInterval){

            timeCounter = 0;

            // ==========================================
            // modify the traffic lights light color
            // ==========================================
            List<TFLStatus> tts = serverResponses[serverResponses.Count - 1].tfls;
            for(int k = 0; k < tts.Count; k++){
                StartCoroutine(changeTrafficLightStatus(tts[k]));
            }

            // ==========================================
            // For each car i currentCars dictionary make
            // changes.
            // ==========================================
            List<carStatus> currList =    serverResponses[serverResponses.Count-1].cars;
            for(int i = 0; i < currList.Count; i++){
                StartCoroutine(MoveCar(currList[i]));
            }

            // ==========================================
            // Make request to get the next position of
            // the cars
            // ==========================================
            StartCoroutine(StepData());

        }
        
    }
}
