using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carMovements : MonoBehaviour
{
    public GameObject theCar;
    public float speed = 0.2f;

    void setInitialPosition(int direction){
        Vector3 initPos0 = new Vector3(385.7f, 9.96f, -1437f);         // up
        Vector3 initPos1 = new Vector3(359.5f, 9.86f, -1097.7f);       // down
        Vector3 initPos2 = new Vector3(544.5f, 9.86f, -1263f);         // left 
        Vector3 initPos3 = new Vector3(195.8f, 9.86f, -1286.3f);       // right

        if(direction == 0){
            theCar.transform.position = initPos0;
            theCar.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
        }
        else if(direction == 1){
            theCar.transform.position = initPos1;
            theCar.transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
        }
        else if(direction == 2){
            theCar.transform.position = initPos2;
            theCar.transform.rotation = Quaternion.Euler(new Vector3(0,270,0));
        }
        else if(direction == 3){
            theCar.transform.position = initPos3;
            theCar.transform.rotation = Quaternion.Euler(new Vector3(0,90,0));
        }
    }

    void moveForwardObject(){
        theCar.transform.position += new Vector3(2,0,0) * Time.deltaTime;
        //theCar.transform.Translate(Vector3.up * Time.deltaTime * speed);
        theCar.transform.position += theCar.transform.forward * Time.deltaTime * speed;
    }

    // Start is called before the first frame update
    void Start()
    {
        setInitialPosition(3);
    }

    // Update is called once per frame
    void Update()
    {
        moveForwardObject();
        
    }
}
