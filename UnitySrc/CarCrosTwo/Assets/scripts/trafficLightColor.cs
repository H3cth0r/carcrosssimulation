using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trafficLightColor : MonoBehaviour
{

    public GameObject stopLight;
    int intensity = 10;

    void changeTrafficLightStatus(int status){
        float emissiveIntensityOn   = 1;
        float emissiveIntensityOff  = 0;

        var mats = stopLight.GetComponent<MeshRenderer>().materials;

        Color ColorGreen            = mats[3].GetColor("_Color");
        Color emissionColorGreen    = mats[3].GetColor("_EmissionColor");
        Color ColorYellow           = mats[1].GetColor("_Color");
        Color emissionColorYellow   = mats[1].GetColor("_EmissionColor");
        Color ColorRed              = mats[2].GetColor("_Color");
        Color emissionColorRed      = mats[2].GetColor("_EmissionColor");


        if(status == 0){        // red
            mats[3].SetColor("_BaseColor", emissionColorGreen);
            mats[3].SetColor("_EmissionColor", emissionColorGreen * emissiveIntensityOn);

            mats[1].SetColor("_BaseColor", ColorYellow);
            mats[1].SetColor("_EmissionColor", ColorYellow * emissiveIntensityOff);

            mats[2].SetColor("_BaseColor", ColorRed);
            mats[2].SetColor("_EmissionColor", ColorRed * emissiveIntensityOff);
        }else if(status == 1){  // Green
            mats[3].SetColor("_BaseColor", ColorGreen);
            mats[3].SetColor("_EmissionColor", ColorGreen * emissiveIntensityOff);

            mats[1].SetColor("_BaseColor", ColorYellow);
            mats[1].SetColor("_EmissionColor", ColorYellow * emissiveIntensityOn);

            mats[2].SetColor("_BaseColor", ColorRed);
            mats[2].SetColor("_EmissionColor", ColorRed * emissiveIntensityOff);

        }else if(status == 2){  // Yellow
            mats[3].SetColor("_BaseColor", ColorGreen);
            mats[3].SetColor("_EmissionColor", ColorGreen * emissiveIntensityOff);

            mats[1].SetColor("_BaseColor", ColorYellow);
            mats[1].SetColor("_EmissionColor", ColorYellow * emissiveIntensityOff);

            mats[2].SetColor("_BaseColor", ColorRed);
            mats[2].SetColor("_EmissionColor", ColorRed * emissiveIntensityOn);

        }

        stopLight.GetComponent<MeshRenderer>().materials = mats;
    }
    // Start is called before the first frame update
    void Start()
    {
        changeTrafficLightStatus(2);
        print("Done");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
